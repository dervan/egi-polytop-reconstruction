from numpy import *
from scipy.spatial import ConvexHull
import scipy.optimize as opt
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math
import visvis as vv
n=0
a = []
v = []
L = []
vInd = []
polytop = []
bcolors={'header':'\033[95m','blue':'\033[94m','green':'\033[92m','yellow':'\033[93m','red':'\033[91m','ENDC':'\033[0m','bold':'\033[1m','ulin':'\033[4m'}

def color(s,form):
    print bcolors[form] + s + bcolors['ENDC']

def normal(v):
    s=0
    for i in v:
        s+=i*i
    return math.sqrt(s)

def f(L):
    global a
    s=0
    for i in range(len(L)):
        s+=a[i]*L[i]
    return s

def gravityCenter(points):
    s=[0]*3
    for p in points:
        s+=p
    s/=len(points)
    return s

def loadData():
    global n,v,a
    n = int(raw_input())
    v=zeros((n,3))
    a=zeros(n)
    for i in range(n):
        s=raw_input().split(' ')
        v[i]=eval(s[0])
        l = normal(v[i])
        v[i]=[j/l for j in v[i]]
        a[i]=float(s[1])*l

    #becaus e vector's set must be fully equilibrated
    w = [0,0,0]
    for i in range(3):
        for j in range(n):
            w[i]+=a[j]*v[j][i]
    if not w[0]==w[1]==w[2]==0:
        l = normal(w)
        n=n+1
        v=append(v,[-j/l for j in w]).reshape((n,3))
        a=append(a,l)

def dualTransform(L,v):
    dual=zeros(v.shape)
    for i in range(len(v)):
        dual[i] = [-t/L[i] for t in v[i]]
    return dual

def drawPoly(points,indices):
    vv.figure()
    g=vv.gca()
    color("Faces:"+str(len(indices)),"green")
    for i in range(len(indices)):
        if (dot(cross(points[indices[i][0]-indices[i][1]],points[indices[i][2]-indices[i][0]]),points[indices[i][0]])<0):
            t=indices[i][1]
            indices[i][1]=indices[i][2]
            indices[i][2]=t
    polytopMesh=vv.mesh(points,indices)
    centroid=gravityCenter(points)
    polytopMesh.faceColor='y'
    polytopMesh.edgeShading='plain'
    polytopMesh.faceShading='plain'
    app=vv.use()
    app.Run()

def computeVolume(w):
    return fabs(dot(w[0],cross(w[1],w[2])))/6.+fabs(dot(w[0]-w[3],cross(w[1]-w[3],w[2]-w[3])))/6.

def V(L):
    global v,vInd,polytop
    volume=0
    w=zeros((4,3))
    for i in range(len(vInd)): # vInd == every vertex
        w[0:3]=[v[vInd[i][j]]*L[vInd[i][j]] for j in range(3)]
        w[3]=polytop[i]
        volume+=computeVolume(w)
    return volume

def gradient(q,x):
    g=zeros(len(x))
    dif= zeros(len(x))
    dif[0]=0.000001
    for i in range(len(x)):
        g[i]=(q(x+dif)-q(x))*10**6
        dif[(i+1)%len(x)]=dif[i]
        dif[i]=0
    return g

def construct():
    global L,polytop,n,v,a,vInd,grad
    dual=dualTransform(L,v)
    hull=ConvexHull(dual)
    dv=[-1.,-1.,-1.]
    hn=len(hull.simplices)
    polytop=zeros((hn,3))
    vInd=hull.simplices
    for i in range(hn):
        lin=dual[hull.simplices[i]]
        polytop[i]=linalg.solve(lin,dv)
def positive(v):
    return all(map(lambda x: x>0,v))

def main():
    global n,v,a,vInd,polytop,L,grad
    loadData()
    L=ones(n) #intialization
    lastf=10**16
    step=ones(n)
    laststep=zeros(n)
    lastL=zeros(n)
    k=0
    while True:
        color("New iteration!"+"*"*30,"red")
        construct()
        shortcut=ConvexHull(polytop)
        #drawPoly(polytop,shortcut.simplices)
        centroid=gravityCenter(polytop)
        print L
        for i in range(len(L)):
            L[i]-=dot(centroid,v[i])
        for i in range(len(polytop)):
            polytop[i]-=centroid
        #color("After shifting:"+str(L),"blue")
        scale=power(V(L),(1/3.))
        L=[l/scale for l in L]
        #color("After scaling:"+str(L),"blue")

        if lastf<f(L): # result isn't better. Try other step
            color("Smaller size! "+str(normal(step))+" "+str(lastf)+" "+str(f(L)),"red")
            L=backupL+step/2
            step=step/2
            if(normal(step)<0.0000001):#close to zero. Try other direction
                print "Reverse!"
                step=-laststep
                while not positive(L+step):
                    step/=2
                L+=step
        else:
            if(fabs(lastf-f(L))<0.00000001):
                break
            else: #better, but not enough to end routine. Next step.
                lastf=f(L)
                grad=gradient(V,L)
                step=-(dot(a,grad)*grad-a)
                k+=1
                while not positive(L+step) and normal(step)>0.000001:
                    step/=2
                if(normal(step)<0.000001):
                    step=(dot(a,grad)*grad-a)
                    while not positive(L+step):
                        step/=2
                laststep=step
                backupL=L
                L+=step
                color("Old f:"+str(f(L)),"yellow")
                '''while f(L+step)>lastf and f(L-step)>lastf:
                #color("Smaller step:"+str(normal(step)),"green")
                step=step/2
                if f(L+step)>f(L-step):
                print("-")
                L-=step
                else:
                print("+")
                L+=step'''

    print v,a
    shortcut=ConvexHull(polytop)
    drawPoly(polytop,shortcut.simplices)

if __name__ == '__main__':
    main()
