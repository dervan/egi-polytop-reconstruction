from polytop import *
from vec import *
import numpy.linalg as lin
from scipy import ndimage

def dummy(tab,a):
    (n,m)=tab.shape
    result=vstack([a*tab[0],tab,a*tab[n-1]])
    result=column_stack([a*result[:,0],result,a*result[:,m-1]])
    return result

def cropDummy(tab):
    (m,n)=tab.shape
    result=delete(tab,[0,n-1],1)
    result=delete(result,[0,m-1],0)
    return result

def crazySquare(squarePolytop,num,shift):
    (a,b,_)=squarePolytop.boundBox()
    squarePolytop.scale(1./a)
    sq=Polytop()
    sq2=Polytop()
    sq.setFaces(zeros((0,3)))
    sq2.setFaces(zeros((0,3)))
    for i in range(num):
        sq.shift([a,0,shift[0]])
        sq.append(squarePolytop)
    for i in range(num):
        sq2.shift([0,b,shift[1]])
        sq2.append(sq)
    sq2.scale(1./num)
    sq2.shift(sq2.centroid())
    return sq2

def ppr(name,matrix):
    printc(name,"red")
    for row in matrix:
        print '| ',
        for x in row:
            print '%.3f\t' % x,
        print '|'

def fixNumber(i):
    j=i-1
    if (j/8)%2:
        return (j/8)*8+(7-j%8)+1
    else:
        return i


def reconstructGradient(dzx,dzy,sigma=0):
    (n,m)=dzx.shape
    dzx=ndimage.gaussian_filter(dzx,sigma)
    dzy=ndimage.gaussian_filter(dzy,sigma)
    dzx=dummy(dzx,0)
    dzy=dummy(dzy,0)
    dz = dzx+dzy
    #dzx=column_stack([[0]*(n+2),vstack([[0]*m,dzx,[0]*m]),[0]*(n+2)])
    #dzy=column_stack([[0]*(n+2),vstack([[0]*m,dzy,[0]*m]),[0]*(n+2)])
    #vstack([dzx[0],dzx,dzx[n-1]]),[0]+dzx[:,m-1]+[0]])
    #dzx=column_stack([[0]+list(dzx[:,0])+[0],vstack([dzx[0],dzx,dzx[n-1]]),[0]+list(dzx[:,m-1])+[0]])
    #dzy=column_stack([[0]+list(dzy[:,0])+[0],vstack([dzy[0],dzy,dzy[n-1]]),[0]+list(dzy[:,m-1])+[0]])
    #dzy=column_stack([[0]*(n+2),vstack([[0]*m,dzy,[0]*m]),[0]*(n+2)])
    gx=zeros((n,m))
    gy=zeros((n,m))
    for i in range(n):
        for j in range(m):
            gx[i,j]=(0.25*dzx[i,j+1]+0.5*dzx[i+1,j+1]+0.25*dzx[i+2,j+1])
            gy[i,j]=(0.25*dzy[i+1,j]+0.5*dzy[i+1,j+1]+0.25*dzy[i+1,j+2])

    #gx=column_stack([[0]*(n+2),vstack([[0]*m,gx,[0]*m]),[0]*(n+2)])
    #gy=column_stack([[0]*(n+2),vstack([[0]*m,gy,[0]*m]),[0]*(n+2)])
    gx=dummy(gx,-1)
    gy=dummy(gy,-1)
    #gx=column_stack([[0]+list((-1)*gx[:,0])+[0],vstack([(-1)*gx[0],gx,(-1)*gx[n-1]]),[0]+list((-1)*gx[:,m-1])+[0]])
    #gy=column_stack([[0]+list((-1)*gy[:,0])+[0],vstack([(-1)*gy[0],gy,(-1)*gy[n-1]]),[0]+list((-1)*gy[:,m-1])+[0]])
    gx=gradient(gx)[0]
    gy=gradient(gy)[1]
    gx=cropDummy(gx)
    gy=cropDummy(gy)
    g = gx+gy
    b = reshape(g,-1)*(-1)
    A = diag([4]*(m*n),0)+diag(([-1]*(m-1)+[0])*(n-1)+[-1]*(m-1),1)+diag(([-1]*(m-1)+[0])*(n-1)+[-1]*(m-1),-1)+diag([-1]*m*(n-1),m)+diag([-1]*m*(n-1),-m)
    for i in range(n*m):
        if i<m-1 or i>(n-1)*m-1 or i%m==0 or i%m==m-1:
            A[i,i]=3
    A[0,0]=3
    A[m*n-1,m*n-1]=2
    A[m-1,m-1]=2
    A[(n-1)*m,(n-1)*m]=2
    u = lin.solve(A,b)
    return reshape(u,(n,m))

def blowUp(tab,b):
    (n,m)=tab.shape
    result = reshape(map(lambda row:list(map(lambda x:[x]*b,row))*b,tab),(-1,b*m))
    return result

def arragment(n,length=8):
    return array([n%length,n/length,0])

def main():
    squareSize=5
    sigma = 15
    n = 4
    m = 8
    dzy =array([[-0.76018202, -0.47243554, -0.26626053, -0.04864259,  0.03569527, 0.32493539,  0.50469432,  0.72148317],
                [-0.73197125, -0.37767279, -0.18403907, -0.15749443,  0.00870242, 0.26398814,  0.47375258,  0.73199808],
                [-0.58840649, -0.38759585, -0.29294056, -0.08770621,  0.12261677, 0.23630029,  0.47973679,  0.67215068],
                [-0.70659066, -0.40990502, -0.32040192, -0.05904414,  0.10187977, 0.246     ,  0.43681826,  0.63735374]])
    dzx =array([[-0.25146855, -0.23055685, -0.25151956, -0.23553254, -0.25561643, -0.25036493, -0.28367716, -0.24522681],
                [ 0.04293787,  0.06937288,  0.05250749,  0.05499297,  0.08735241, 0.06195413,  0.07052377,  0.04512687],
                [ 0.30064004,  0.29624539,  0.29695815,  0.28084048,  0.31455155, 0.32574661,  0.27966463,  0.27239382],
                [ 0.53060855,  0.50732619,  0.48013271,  0.50575451,  0.48526269, 0.52931972,  0.50544935,  0.48398781]])
    for i in range(n):
        for j in range(m):
            dzx[i,j]*=0.5 #SCALING!
            dzy[i,j]*=0.5
    dz=dzx+dzy
    sdzx=blowUp(dzx,squareSize)
    sdzy=blowUp(dzy,squareSize)
    z = reconstructGradient(sdzx,sdzy,sigma)
    u = reshape(z,-1)

    mainShapes = {}
    dzx=reshape(dzx,-1)
    dzy=reshape(dzy,-1)
    dz=reshape(dz,-1)
    for i in range(1,33):
        name = "sol"+str(i)+".obj"
        fi = fixNumber(i)-1

        f = open(name,'r')
        v = zeros((0,3))
        indices = zeros((0,3))

        for line in f:
            vals = line.split(' ')
            if vals[0]=='v':
                v=append(v,[map(float,vals[1:4])],axis=0)
            if vals[0]=='f':
                indices=append(indices,[map(lambda x: int(x)-1,vals[1:4])],axis=0)

        mainShapes[fi] = Polytop(v)
        mainShapes[fi].setFaces(indices)
        mainShapes[fi].fixNormals()
        (a,_,_)=mainShapes[fi].boundBox()
        mainShapes[fi].scale(1./a)
        f.close()
    result = Polytop(zeros((0,3)))
    result.setFaces(zeros((0,3)))

    for i in range(32):
        i1=i%8
        i2=i/8
        square=Polytop()
        for j in range(squareSize):
            for k in range(squareSize):
               tmp=Polytop()
               tmp.append(mainShapes[i])
               tmp.shift([k,j,u[((i2*squareSize+j)*8+i1)*squareSize+k]])
               square.append(tmp)
        square.scale(1./squareSize)
        square.shift([i%8,i/8,0])
        result.append(square)
    result.save("curved"+str(squareSize)+"-"+str(sigma)+".obj")
    result.draw()




if __name__ == '__main__':
    main()
