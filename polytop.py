from numpy import *
from vec import *
from scipy.spatial import ConvexHull
import math
import visvis as vv


bcolors={'header':'\033[95m','blue':'\033[94m','green':'\033[92m','yellow':'\033[93m','red':'\033[91m','ENDC':'\033[0m','bold':'\033[1m','ulin':'\033[4m'}

def printc(s,form,nl=1):
    print bcolors[form],s, bcolors['ENDC'],
    if nl:
        print

class Polytop:
    vertices = []
    indices = []
    normals = []
    areas = []
    volume = 0
    n = 0
    faceno = 0
    L = []

    def __init__(self,vertices=zeros((0,3))):
        self.n = len(vertices)
        self.vertices=vertices
        self.indices=zeros((0,3),dtype=int)

    #appending one polytop to another one
    def append(self,poly):
        self.vertices = append(self.vertices,poly.vertices,axis=0)
        self.indices = append(self.indices,poly.indices+self.n,axis=0)
        self.n+=poly.n

    def computeFaces(self):
        self.indices=ConvexHull(self.vertices).simplices
        self.areas=zeros(self.faceno)

    def setFaces(self,faces):
        self.indices=faces
        self.areas=zeros(self.faceno)

    def setNormals(self,normals):
        self.faceno=len(normals)
        self.normals=reshape(normals,(self.faceno,3))

    def centroid(self):
        return average(self.vertices,axis=0) 

    #Work if centeroid of polytop is in center of coordinate system.
    def calcVolume(self):
        self.volume=0
        for face in self.indices:
            #Because here I assume that one point is (0,0,0)
            self.volume+=tetrahedraVolume(self.vertices[face])

    def fixNormals(self):
        for j,i in enumerate(self.indices):
            v1=self.vertices[i[0]]-self.vertices[i[1]]
            v2=self.vertices[i[2]]-self.vertices[i[1]]
            m = cross(v1,v2)
            if dot(m,(self.vertices[i[1]]+self.vertices[i[0]]+self.vertices[i[2]]))<0:
                tmp=self.indices[j][0]
                self.indices[j][0]=self.indices[j][1]
                self.indices[j][1]=tmp


    #Awful. I'm sorry.
    def calcAreas(self):
        for j,i in enumerate(self.indices):
            v1=self.vertices[i[0]]-self.vertices[i[1]]
            v2=self.vertices[i[2]]-self.vertices[i[1]]
            m = cross(v1,v2)
            m = m/norm(m)
            if dot(m,self.vertices[i[1]]+self.vertices[i[0]]+self.vertices[i[2]])<0:
                m=-m
            if m[0]==-0:
                m[0]=0
            if m[1]==-0:
                m[1]=0
            area=0.5*norm(cross(v1,v2))
            self.addArea(self.areas,area,m)

    def addArea(self,newa,area,normal):
        added=False
        mn=1
        for i,w in enumerate(self.normals):
            if norm(w-normal)<10**(-7):
                added=True
                newa[i]+=area
                break
            else:
                if mn>(norm(w-normal)):
                    mn=norm(w-normal)
        if not added:
            printc("There is no normal "+str(normal),"red")

    def shift(self,val):
        for v in range(self.n):
            self.vertices[v]+=val

    def scale(self,s):
        for v in range(self.n):
            self.vertices[v]*=s

    def save(self,filename):
        f = open(filename, 'w')
        for p in self.vertices:
            f.write("v ")
            for pp in p:
               f.write(str(pp)+" ")
            f.write('\n')
        for i in self.indices:
            f.write("f ")
            for ii in i:
                f.write(str(int(ii)+1)+" ")
            f.write('\n')
        f.close()

    def printObj(self):
        for p in self.vertices:
            print "v ",
            for pp in p:
               print pp,
            print
        for i in self.indices:
            print "f ",
            for ii in i:
                print ii+1,
            print

    def boundBox(self):
        return amax(self.vertices,axis=0)-amin(self.vertices,axis=0)

    def stats(self,referenceSizes,infos):
        if infos&1:
            print("Triangles: ",self.n)
        newa=zeros(self.faceno)
        for i in self.indices:
            v1=self.vertices[i[0]]-self.vertices[i[1]]
            v2=self.vertices[i[2]]-self.vertices[i[1]]
            normal = cross(v1,v2)
            normal = normal/norm(normal)
            if dot(normal,self.vertices[i[1]]+self.vertices[i[0]]+self.vertices[i[2]])<0:
                normal=-normal
            if normal[0]==-0:
                normal[0]=0
            if normal[1]==-0:
                normal[1]=0
            area=0.5*norm(cross(v1,v2))
            if infos&1:
                print "Norm: ",normal,
                print "Area: ", area
            self.addArea(newa,area,normal)
        ratios=referenceSizes/newa
        scale=average(ratios)
        difs=1-newa*scale/referenceSizes
        if infos&2:
            print("Faces: ",self.faceno)
            for aa in zip(referenceSizes,newa*scale,map(lambda x:str(round(100*x,2))+"%",difs),self.normals):
                printc(str(aa),"yellow")
        if infos&4:
            print map(lambda x:str(round(100*x,2))+"%",difs)
        if infos&8:
            print "Max % difference: ",round(100*max(map(fabs,difs)),2),"%"
            printc("Standard ratios deviation:","blue",0)
            printc(std(ratios),"blue")
            printc("Maximal deviation:","blue",0)
            printc(max(map(fabs,difs)),"blue")

    def draw(self):
        vv.figure()
        vv.gca()
        polytopMesh=vv.mesh(self.vertices,self.indices)
        polytopMesh.faceColor='y'
        polytopMesh.edgeShading=None
        polytopMesh.faceShading='flat'
        app=vv.use()
        app.Run()
