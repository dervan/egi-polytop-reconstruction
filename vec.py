from numpy import *
import math

def positive(v):
    return all(map(lambda x: x>0,v))

def norm(v):
    s=0
    for i in v:
        s+=i*i
    return math.sqrt(s)

def normalize(v):
    return [a/norm(v) for a in v]

def gravityCenter(points):
    s=[0]*3
    for p in points:
        s+=p
    s/=len(points)
    return s

def contrary(v):
    return [-a for a in v]

def dualTransform(L,v):
    dual=zeros(v.shape)
    for i in range(len(v)):
        dual[i] = [-t/L[i] for t in v[i]]
    return dual

def tetrahedraVolume(w):
    return fabs(dot(w[0],cross(w[1],w[2])))/6.#+fabs(dot(w[0]-w[3],cross(w[1]-w[3],w[2]-w[3])))/6.

def sign(x):
    if x>0:
        return 1
    elif x<0:
        return -1
    else:
        return 0

def decompose(v):
    return zip([[sign(v[0]),0,0],[0,sign(v[1]),0],[0,0,sign(v[2])]],map(fabs,v))
    vx=[v[0],0,0]
    vy=[0,v[1],0]
    vz=[0,0,v[2]]

def equalization(vs,a):
    res=zeros(3)
    for j,v in enumerate(vs):
        res-=v*a[j]
    return res

def disasseble(vs,a,v1,v2):
    res=zeros((2,2))
    for j,v in enumerate(vs):
        for i,w in [(0,v1),(1,v2)]:
            if dot(v,w)>0:
                res[0][i]+=dot(v,w)*a[j]
            else:
                res[1][i]-=dot(v,w)*a[j]
    return res


def extend(n,v,a,newv,newa):
    for i in range(len(newv)):
        found = 0
        for j,vv in enumerate(v):
            if norm(newv[i]-vv)<10**(-9):
                found = True
                a[j]+=newa[i]
        if not found and norm(newv[i])>10**(-8):
            n=n+1
            v=append(v,newv[i]).reshape((n,3))
            a=append(a,newa[i])
    return(n,v,a)

def projection(v,n):
    return dot(v,n)*n/norm(n)**2

def rejection(v,n):
    return v-projection(v,n)

