from numpy import *
from polytop import *
from vec import *
from scipy.spatial import ConvexHull
import scipy.optimize as opt
import math
import pprint as pp
import copy as cp
from random import randint
import sys

def f(L,a):
    s = 0
    for i in range(len(L)):
        s+=L[i]*a[i]
    return s

def loadData():
    n = int(raw_input())
    v=zeros((n,3))
    a=zeros(n)
    for i in range(n):
        s=raw_input().split(' ')
        map(eval,s)
        v[i]=s[0:3]
        l = norm(v[i])
        v[i]=[j/l for j in v[i]]
        a[i]=float(s[3])*l
    return (n,v,a)

def construct(L,v):
    dual=dualTransform(L,v)
    hull=ConvexHull(dual)
    dv=[-1.,-1.,-1.]
    hn=len(hull.simplices)
    points=zeros((hn,3))
    for i in range(hn):
        lin=dual[hull.simplices[i]]
        points[i]=linalg.solve(lin,dv)
    poly=Polytop(points)
    poly.setNormals(v)
    poly.computeFaces()
    return poly

def reconstruct(n,v,a,initL,eps):
    #Step 1.
    L=initL
    lastf=10**16
    step=ones(n)
    lastL=zeros(n)
    k=0

    #This fancy factor improves speed of convergence if you want large precision. Experimental.
    alpha = 1

    #alpha is increased only if last step was good (shortening not needed)
    unchanged=False
    while True:
        if(norm(step)<10**(-16)):
            print("Zero step size! Terminating...")
            break
        #Step 2.
        poly=construct(L,v)
        #Step 3.
        centroid=poly.centroid()
        for i in range(len(L)):
            L[i]-=dot(centroid,v[i])
        poly.shift(-centroid)
        poly.calcVolume()
        poly.calcAreas()
        scale=power(poly.volume,(1/3.))
        L=[l/scale for l in L]
        #Step 4.
        if lastf<f(L,a):
            # Result isn't better. Try other step.
            print "#",
            alpha=1.1*alpha
            step=step/1.2
            L=backupL+step
            unchanged=False
        else:
            if(fabs(lastf-f(L,a))<eps):
                #  Cool enough. Terminating
                print "\nIterations: ",k,
                printc("Result found succesfully","green")
                break
            else:
                #  Result iprovement
                print "\n",k," f:",
                printc(f(L,a),"red",0)
                printc(lastf-f(L,a),"red",0)
                lastf=f(L,a)
                poly=construct(L,v)
                poly.calcAreas()
                grad=poly.areas*(1/3.)
                grad=grad/norm(grad)
                step=(dot(a,grad)*grad-a)
                while not positive(L+step) and norm(step)>2**(-16):
                    step=step/1.2
                k+=1
                if unchanged:
                    alpha=alpha/1.1
                #initial magnitude of step
                step=step/(1.+alpha*k)
                backupL=L
                L+=step
                unchanged=True
    poly.L=L
    return poly


def squareOptimization(on,ov,oa,eps):
    w = equalization(ov,oa)
    # on+5 = on + contrary + square-making vectors
    L=ones(on+5)
    ux = [1,0,0]#rejection([1,0,0],w)
    uy = [0,1,0]#rejection([0,1,0],w)
    xlen = 1
    ylen = 0
    ax = 1
    ay = 1
    while fabs(xlen-ylen)>0.001:
        print (ax,ay),"=> ",
        (n,v,a) = extend(on,ov,oa,map(normalize,[w,ux,uy,contrary(ux),contrary(uy)]),[norm(w),ax,ay,ax,ay])
        poly = reconstruct(n,v,a,L,eps)
        L = poly.L
        (xlen,ylen,_)=poly.boundBox()
        printc((xlen,ylen,xlen/ylen),"red")
        poly.stats(a,8)
        if xlen>ylen:
            ax+=0.5*(xlen-ylen)
        else:
            ay+=0.5*(ylen-xlen)
        print ".:"*30
    if len(sys.argv)<2:
        name = "solution.obj"
    else:
        name="sol"+sys.argv[1]+".obj"
    print "Saving file: "+name
    poly.draw()
    poly.save(name)

def main():
    eps=10**(-8)
    (on,ov,oa)=loadData()
    #if you want square shape, only:
    #squareOptimization(on,ov,oa,eps)
    #and return
    w = equalization(ov,oa)
    (n,v,a) = extend(on,ov,oa,[normalize(w)],[norm(w)])
    L = ones(n)
    poly = reconstruct(n,v,a,L,eps)
    poly.draw()
    poly.stats(a,4)


if __name__ == '__main__':
    main()
