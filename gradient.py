from polytop import *
from vec import *

if __name__ == '__main__':
    grad=zeros((32,2))
    for i in range(1,33):
        name = "normals/normals_areas_"+str(i)+".txt"
        f = open(name,'r')
        v = zeros((0,3))
        indices = zeros((0,3))
        n = int(f.readline())-1
        for j,line in enumerate(f):
            if j==n:
                (x,y,z,a) = map(float,line.split(' '))
                grad[i-1]= (2*dot([x,y,z],[1,0,0])/norm(cross([x,y,z],[1,0,0])),2*dot([x,y,z],[0,1,0])/norm(cross([x,y,z],[0,1,0])))
        f.close()
    print repr(grad)
